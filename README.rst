
A set of various useful tools and custom libraries, which don't
require a huge amount of dependencies (mostly standard library only).

Compatibility
-------------

**Python**: 3.6, 3.7, 3.8, 3.9, pypy3.6

Summary
-------

- **config** - configuration utilities (CLI args, environment loaders etc.)
 - **class_registry.py** - class registry for dynamic object generation
 - **exceptions.py** - base serializable exception and default exception classes
 - **jsonschema.py** - json schema wrapper classes and functions
 - **logging.py** - base logging class and default logging settings
 - **mapping.py** - various dictionary operations
 - **serialization.py** - serializable and hashable classes
 - **services.py** - service base classes and service registry
   compatible with aiohttp webapp cleanup context
 - **threading.py** - threading and async threading operations
 - **ttl_dict.py** - simple TTL dictionary implementation

Testing
-------

At first, you must install `requirements.tests.txt` as well
as normal requirements.

pytest
^^^^^^

Run `pytest` command. There's also a Pycharm *unittests*
run configuration ready to use.

tox
^^^

To test with tox you should install and use `pyenv`. First
setup local interpreters which you want to use in tests.

```pyenv global 3.7.5 3.8.0```

Then you can run `tox` command to test against all of them.

coverage
^^^^^^^^

Don't use PyCharm bundled coverage. Because it doesn't
ignore module docstrings you get wrong results.

Documentation
-------------

Install `requirements.docs.txt` and then
cd to `./docs` and run `make html` command. There is also a
run configuration for Pycharm.
