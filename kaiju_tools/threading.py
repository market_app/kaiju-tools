"""
Various threading operations.

Usage
-----

Use `async_run_in_thread` for wrapping synchronous functions for async execution.

.. code-block:: python

    import asyncio
    from kaiju_tools.threading import async_run_in_thread

    def my_func(filename):
        with open(filename, 'w') as f:
            f.write('something')


    loop = asyncio.get_event_loop()
    f = async_run_in_thread(my_func, 'file.txt') # not it can be awaited
    loop.run_until_complete(f)


You also can set up a `max_exec_time` for a function execution.

*CAUTION:* Because terminating a thread is a tricky operation,
there is no guarantees that it will be actually terminated on timeout.
Use it on your own risk.

.. code-block:: python

    import asyncio
    from kaiju_tools.threading import async_run_in_thread

    def my_func():
        while True:
            1 + 1

    loop = asyncio.get_event_loop()
    f = async_run_in_thread(my_func, max_exec_time=1)  # will try to terminate in one second
    loop.run_until_complete(f)


Functions
---------

"""

import asyncio
import ctypes
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import TimeoutError as ConcurrentTimeoutError
from functools import partial

__all__ = (
    'terminate_thread', 'async_run_in_thread'
)


def terminate_thread(thread):
    """Terminates a python thread from another thread.

    Found it on stack overflow as an only real way to stop a stuck python thread.
    http://code.activestate.com/recipes/496960-thread2-killable-threads/
    Use with caution.

    :param thread: a threading.Thread instance
    """

    if not thread.isAlive():
        return

    exc = ctypes.py_object(SystemExit)
    res = ctypes.pythonapi.PyThreadState_SetAsyncExc(
        ctypes.c_long(thread.ident), exc)
    if res == 0:
        raise ValueError("nonexistent thread id")
    elif res > 1:
        # """if it returns a number greater than one, you're in trouble,
        # and you should call it again with exc=NULL to revert the effect"""
        ctypes.pythonapi.PyThreadState_SetAsyncExc(thread.ident, None)
        raise SystemError("PyThreadState_SetAsyncExc failed")


async def async_run_in_thread(f, *args, max_exec_time: float = None, **kws):
    """Run a function in a separate thread as an async function.

    :param f: function
    :param args: function arguments
    :param kws: function arguments
    :param max_exec_time: max execution time in seconds
    :return: function result
    """

    loop = asyncio.get_event_loop()
    f = partial(f, *args, **kws)

    with ThreadPoolExecutor(max_workers=1) as tp:
        future = loop.run_in_executor(tp, f)
        try:
            result = await asyncio.wait_for(future, max_exec_time, loop=loop)
        except ConcurrentTimeoutError:
            tp.shutdown(wait=False)
            for t in tp._threads:
                terminate_thread(t)
            raise
        else:
            return result
