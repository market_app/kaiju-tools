

def tuple_splitter(flat_key):
    return flat_key


def dot_splitter(flat_key):
    keys = tuple(flat_key.split('.'))
    return keys


def underscore_splitter(flat_key):
    keys = tuple(flat_key.split("_"))
    return keys