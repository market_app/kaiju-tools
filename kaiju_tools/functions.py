from asyncio import sleep

__all__ = ('retry', )


async def retry(
        func, *args, retries: int = 1, retry_timeout: float = 0.25,
        exception_classes=(ConnectionError, TimeoutError),
        logger=None, **kws):
    """Repeats an asynchronous connection (or any operation which requires a
    connection) multiple times if any connection error occurs.

    :param func: an asynchronous function
    :param retries: max number of retries (-1 for infinite)
    :param retry_timeout: timeout in sec between tries
    :param exception_classes: valid exception classes for retrying
    :param args: function arguments
    :param kws: function keywords
    :param logger: optional logger class

    :raises StopIteration: sometimes if it can't catch the last exception (for
        example if zero repeats was provided)
    """

    exc = None
    max_timeout = 10
    retry_timeout_mod = 1.25
    retry_timeout = min(retry_timeout, max_timeout)

    if retries == -1:
        retries = float('Inf')

    while retries:
        try:
            result = await func(*args, **kws)
        except Exception as err:
            if isinstance(err, exception_classes) or err.__class__.__name__ in exception_classes:
                if logger:
                    logger.info('Connection failed due to %s. Retrying...', err.__class__.__name__)
                exc = err
                await sleep(retry_timeout)
                retry_timeout = min(max_timeout, retry_timeout * retry_timeout_mod)
                retries -= 1
                continue
            else:
                raise
        else:
            return result
    else:
        if exc:
            raise exc
        else:
            raise StopIteration
