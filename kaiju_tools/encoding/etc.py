
__all__ = ('MimeTypes',)


class MimeTypes:
    """
    Standard message type headers.
    """

    json = 'application/json'
    msgpack = 'application/msgpack'
