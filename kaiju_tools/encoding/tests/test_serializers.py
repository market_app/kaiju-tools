import pytest

from ..serializers import serializers


@pytest.mark.unit
def test_serializers(logger):
    msg = {'value': True}
    s = serializers['application/json'].dumps(msg)
    _msg = serializers['application/json'].loads(s)
    assert msg == _msg
