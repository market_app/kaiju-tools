"""
Contains some useful functions for working with dictionaries and collections
of dictionaries.

Functions
---------

"""

import uuid

from collections import defaultdict
from itertools import zip_longest
from typing import Iterable

import multidict
from datemath import dm

__all__ = (
    'strip_fields', 'flatten', 'unflatten', 'equal',
    'recursive_update', 'filter_fields', 'extract_field',
    'set_field', 'to_multidict', 'fill_template'
)


def strip_fields(obj, prefix: str = '_'):
    """
    Strips unwanted fields from a dictionary or a list of dictionaries. You can
    customize a key prefix (default is '_').

    .. code-block:: python

        from kaiju_tools.mapping import strip_fields

        strip_fields({'_meta': 123, 'name': 'bob'})  # will remove '_meta' field
        strip_fields([{'a': 1, '.b': 2}, {'c': 3, '.b': 4}], prefix='.')  # will remove .b from each element

    """

    if isinstance(obj, dict):
        new = {}
        for key, value in obj.items():
            if not key.startswith(prefix):
                new[key] = strip_fields(value, prefix=prefix)
        return new
    elif isinstance(obj, Iterable) and not isinstance(obj, str):
        return [strip_fields(sub, prefix=prefix) for sub in obj]
    else:
        return obj


def flatten(data, delimiter='.') -> dict:
    """
    Flattens dictionary keys according to the specified delimiter (default to '.').

    .. code-block:: python

        from kaiju_tools.mapping import flatten

        flatten({'a': {'b': {'c': 1, 'd': 2}}})
        # will return {'a.b.c': 1, 'a.b.d': 2}

    """

    if isinstance(data, dict):
        _data = {}
        for key, value in data.items():
            prefix = f'{key}{delimiter}'
            if isinstance(value, dict):
                value = flatten(value, delimiter=delimiter)
                for k, v in value.items():
                    k = f'{prefix}{k}'
                    _data[k] = v
            elif isinstance(value, Iterable) and not isinstance(value, str):
                _data[key] = [flatten(obj, delimiter=delimiter) for obj in value]
            else:
                _data[key] = value
        return _data
    elif isinstance(data, Iterable) and not isinstance(data, str):
        return [flatten(obj) for obj in data]
    else:
        return data


def unflatten(data: dict, delimiter='.') -> dict:
    """
    Unflattens a flattened dict into a folded dict.

    .. code-block:: python

        from kaiju_tools.mapping import unflatten
        unflatten({'a.b.c': True, 'a.c.d': False, 'e': True})
        # will return {'a': {'b': {'c': True}, 'c': {'d': False}}, 'e': True}

    """

    _data = {}

    for key, value in data.items():
        key = key.split(delimiter)
        _d = _data
        for k in key[:-1]:
            if k in _d:
                _d = _d[k]
            else:
                _new_d = {}
                _d[k] = _new_d
                _d = _new_d
        _d[key[-1]] = value

    return _data


def recursive_update(obj1, obj2) -> dict:
    """
    Recursively updates dictionary from another dictionary.

    .. code-block:: python

        from kaiju_tools.mapping import recursive_update

        recursive_update({'a': {'b': 1}}, {'a': {'c': 2}, 'd': 3})
        # will return {'a': {'b': 1, 'c': 2}, 'd': 3}

    """

    if isinstance(obj1, dict):
        if isinstance(obj2, dict):
            for key, value in obj2.items():
                if key in obj1:
                    obj1[key] = recursive_update(obj1[key], value)
                else:
                    obj1[key] = value
        else:
            obj1 = obj2
    elif isinstance(obj1, Iterable) and not isinstance(obj1, str):
        result = []
        if isinstance(obj2, Iterable) and not isinstance(obj2, str):
            for o1, o2 in zip_longest(obj1, obj2):
                if o1 is not None:
                    if o2 is not None:
                        result.append(recursive_update(o1, o2))
                    else:
                        result.append(o1)
                else:
                    result.append(o2)
        obj1 = result
    else:
        obj1 = obj2

    return obj1


def _filter_field(obj, keys):
    for n, key in enumerate(keys):
        if isinstance(obj, dict):
            return {key: _filter_field(obj.get(key), keys[n+1:])}
        elif isinstance(obj, Iterable) and not isinstance(obj, str):
            return [_filter_field(o, keys[n:]) for o in obj]
    return obj


def filter_fields(obj, fields: Iterable[str], delimiter='.') -> dict:
    """
    Filter data according to the specified fields. If specified field doesn't exist
    in the mapping, it will be set to `None` in the result. List values are also
    supported.

    .. code-block:: python

        from kaiju_tools.mapping import filter_fields

        filter_fields({'a': {'b': 1, 'c': 2}}, fields=['a.b', 'a.d'])
        # will return {'a': {'b': 1, 'd': None}}
        filter_fields({'a': [{'b': 1, 'c': 2}, {'b': 2, 'c': 3}]}, fields=['a.b'])
        # will return {'a': [{'b': 1}, {'b': 2}]}

    """

    result = {}
    for field in fields:
        result = recursive_update(result, _filter_field(obj, field.split(delimiter)))
    return result


def extract_field(obj: dict, key: str, default=KeyError, delimiter='.'):
    """
    Extracts field from a folded dictionary. You may specify a default value or
    default exception class.

    .. code-block:: python

        from kaiju_tools.mapping import extract_field

        extract_field({'a': {'b': 1, 'c': 2}}, 'a.b') # will return 1

    """

    if key in obj:
        return obj[key]

    for _key in key.split(delimiter):
        if _key:
            if _key in obj:
                obj = obj[_key]
            else:
                if type(default) is type:
                    if issubclass(default, Exception):
                        raise default(key)
                return default
    return obj


def set_field(obj: dict, key: str, value, delimiter='.'):
    """
    Sets field value in a folded dictionary.

    .. code-block:: python

        obj = {'data': {}}
        set_field(obj, 'data.shite.name', True)
        # obj will be modified
        # {'data': {'shite': {'name': True}}}

    :param obj: a dictionary
    :param key: a key
    :param value: value to set
    :param delimiter:
    """

    key = key.split(delimiter)
    for _key in key[:-1]:
        if _key not in obj:
            obj[_key] = {}
        obj = obj[_key]
    obj[key[-1]] = value


def to_multidict(kwargs: dict) -> multidict.MultiDict:
    """Converts dictionary into aiohttp `MultiDict` object."""

    md = multidict.MultiDict()
    for k, v in kwargs.items():
        if isinstance(v, list):
            for _v in v:
                md.add(k, _v)
        else:
            md.add(k, v)
    return md


def equal(value_1: dict, value_2: dict) -> bool:
    """
    Compares two dictionaries by flattening them and comparing the values.

    >>> equal({'a': 4, 'd': {'b': 2, 'c': 3}}, {'d': {'b': 2, 'c': 3}, 'a': 4})
    True

    >>> equal({'a': 4, 'd': {'b': 2, 'c': 3}}, {'d': {'b': 2, 'c': 3, 'e': 4}, 'a': 4})
    False

    """

    value_1, value_2 = flatten(value_1), flatten(value_2)

    for k in set(value_1) | set(value_2):
        if k not in value_1 or k not in value_2:
            return False
        elif value_1[k] != value_2[k]:
            return False
    else:
        return True


def _format_default():
    return 'NOT_SET'


template_procedures = {

    'uuid4': uuid.uuid4

}


def fill_template(template, data: dict, default=KeyError, skip_fields=None):
    """
    This is a very simple template engine capable of some options.

    It accepts a template (usually a string, dict or list) and a mapping of
    template values. There are some rules for templates:

        "key": "[value]"    // simple template value
        "key": "[obj.key2]" // folded values should be separated by dots
        "key": "[dm:now+1h]"  // datemath template starts with 'd[...]'
        "key": "[f:Some {string}.]" // a template for formatted strings starts with 'f[...]'
        "key": "[proc:procedure_name]"  // replace by a procedure call

    Available procedures:

        proc:uuid4 - random UUID

    For the datemath format reference see
    `https://www.elastic.co/guide/en/elasticsearch/reference/current/common-options.html#date-math`

    .. note::

        Values inserted are of the same type as in a provided data map. If you want
        any to string conversion, then use a formatted string template syntax like: "f[{value}]".


    See an example below:

    >>> fill_template(
    ...     template={
    ...         "id": "[client_id]", "tag": "testing",
    ...         "name": "[f:Hello, {some_name}!]",
    ...         "l": ["[enemies.a]", "[enemies.b]", "[enemies.a]"],
    ...         "folded": {"t": "[not]"}
    ...     },
    ...     data={
    ...         "client_id": 43,
    ...         "some_name": "shiteman",
    ...         "not": "NOT",
    ...         "enemies": {"a": "Sobaki", "b": "Koty", "c": "Slon"}
    ...     },
    ...     skip_fields=["t"]
    ... )
    {'id': 43, 'tag': 'testing', 'name': 'Hello, shiteman!', 'l': ['Sobaki', 'Koty', 'Sobaki'], 'folded': {'t': '[not]'}}

    :param template: template
    :param data: data map
    :param default: default value or default error code if a template value can't be resolved
    :param skip_fields: allows to skip inner fields (for example if there is an folded template
        which shouldn't be filled)
    """

    if isinstance(template, dict):
        t = {}
        if skip_fields:
            for key, value in template.items():
                if key in skip_fields:
                    t[key] = value
                else:
                    t[key] = fill_template(value, data, default=default, skip_fields=skip_fields)
        else:
            for key, value in template.items():
                t[key] = fill_template(value, data, default=default, skip_fields=skip_fields)
        template = t

    elif isinstance(template, str):

        if template.startswith('[') and template.endswith(']'):

            template = template.strip('[]')

            if template.startswith('d:'):
                template = template.replace('d:', '', 1)
                template = str(dm(template).date())
            elif template.startswith('f:'):
                template = template.replace('f:', '', 1)
                template = template.format_map(defaultdict(_format_default, data))
            elif template.startswith('proc:'):
                template = template.replace('proc:', '', 1)
                template = template_procedures[template]()
            else:
                template = extract_field(data, template, default=default)

    elif isinstance(template, Iterable):
        template = [
            fill_template(value, data, default=default, skip_fields=skip_fields)
            for value in template
        ]

    return template
