import abc
import uuid
from typing import *

from . import jsonrpc
from .abc import AbstractRPCCompatible
from .etc import JSONRPCHeaders
from ..mapping import extract_field, set_field
from ..exceptions import APIException
from ..services import ContextableService

__all__ = (
    'RPCClientError', 'RPCClientService', 'BaseRPCClientService',
    'AbstractTokenInterface', 'AbstractRPCTransportInterface'
)


class RPCClientError(APIException):
    """JSON RPC Python exception class. Catch it if you can!"""

    def __init__(self, *args, response=None, **kws):
        super().__init__(*args, **kws)
        self.response = response

    def __str__(self):
        return self.message


class AbstractRPCTransportInterface(abc.ABC):
    """
    This interfaces describes methods of a transport to be able to be used by
    the :class:`.AbstractRPCClientService`.
    """

    @abc.abstractmethod
    async def rpc_request(
            self, uri: str,
            request: Union[jsonrpc.RPCRequest, List[jsonrpc.RPCRequest]],
            headers: Optional[Dict[str, Any]]) -> Union[dict, list]:
        """
        This method depends on a specific transport.

        :param uri: RPC endpoint, queue name etc.
        :param request: request body (JSON compatible)
        :param headers: optional request headers
        """


class AbstractTokenInterface(abc.ABC):
    """
    This interface describes a token provider service methods to be able to be
    used by the :class:`.AbstractRPCClientService`.
    """

    @abc.abstractmethod
    async def get_token(self) -> str:
        """Must always return a valid auth token."""


class BaseRPCClientService(ContextableService, abc.ABC):
    """
    This is an abstract RPC client service which provides methods for creating and
    parsing valid RPC request and request batches. There is also an RPC interface
    for the `call` method.

    To configure the client for your own transport you must set your own
    `init_session` and `close_session` and `rpc_request` methods.

    :param app: web app
    :param transport: message transport service
    :param token_service: a service which provides valid access tokens
    :param uri: RPC service default endpoint
    :param logger: logger instance
    """

    service_name = 'client'
    token_service_class = AbstractTokenInterface
    headers = JSONRPCHeaders

    def __init__(
            self, app,
            token_service: Union[str, token_service_class] = False,
            uri: str = '/rpc',
            logger=None):

        super().__init__(app=app, logger=logger)
        self._token_service = self.discover_service(token_service, required=False)
        self._uri = uri

    @classmethod
    def _process_response_batch(cls, batch: List[dict], unpack_response: bool):
        errors, responses = [], []
        for data in batch:
            response = cls._process_response(data, unpack_response)
            if isinstance(response,  jsonrpc.RPCError):
                errors.append(response)
            responses.append(response)
        return errors, responses

    @staticmethod
    def _process_response(response: dict, unpack_response: bool):
        if 'error' in response:
            error = response['error']
            error_type = error.get('data', {}).get('type', jsonrpc.RPCError.__name__)
            error_type = getattr(jsonrpc, error_type, jsonrpc.RPCError)
            response = error_type(id=response['id'], data=error)
        else:
            response = jsonrpc.RPCResponse(**response)
            if unpack_response:
                response = response.result
        return response

    def _raise_rpc_error(self, error, errors, responses):
        message = str(error)
        if 'base_exc_message' in error.data:
            base_type = error.data['base_exc_type']
            msg = error.data["base_exc_message"]
            message = f'{message} [{base_type}] {msg}'
        self.logger.error(message)
        raise RPCClientError(message, errors=errors, responses=responses)

    async def _set_headers(self, headers: Optional[dict]) -> Optional[dict]:
        """Writes an auth token to the headers if required."""

        if headers is None:
            headers = {}

        if self._token_service:
            if 'Authorization' in headers:
                return headers
            token = await self._token_service.get_token()
            headers[self.headers.AUTHORIZATION] = f'Bearer {token}'

        correlation_header = self.headers.CORRELATION_ID_HEADER

        if correlation_header not in headers:
            headers[correlation_header] = str(uuid.uuid4())

        if self.app:
            headers[self.headers.APP_ID_HEADER] = self.app.id

        headers[self.headers.CONTENT_TYPE_HEADER] = 'application/json'

        return headers


class RPCClientService(BaseRPCClientService, AbstractRPCCompatible):

    transport_service_class = AbstractRPCTransportInterface

    def __init__(self, *args, transport: Union[str, transport_service_class], **kws):
        super().__init__(*args, **kws)
        self._transport = self.discover_service(transport)

    @property
    def routes(self) -> dict:
        return {
            'call': self.call
        }

    @property
    def permissions(self) -> dict:
        return {
            '*': self.PermissionKeys.GLOBAL_SYSTEM_PERMISSION
        }

    async def iter_call(
            self, method: str, params: Any = None, headers: dict = None,
            offset: int = 0, limit: int = 10,
            count_key='count', offset_key='offset', limit_key='limit',
            raise_exception=True, unpack_response=True) -> AsyncGenerator:
        """Iterates over data."""

        count = offset + 1

        if params is None:
            params = {}

        while count > offset:
            set_field(params, offset_key, offset)
            set_field(params, limit_key, limit)
            headers = await self._set_headers(headers)
            data = await self.call(
                method=method, params=params, headers=headers,
                raise_exception=raise_exception, unpack_response=unpack_response)
            if type(data) is jsonrpc.RPCResponse:
                count = extract_field(data.result, count_key, default=0)
            elif isinstance(data, jsonrpc.RPCError):
                count = 0
            else:
                count = extract_field(data, count_key, default=0)
            yield data

            offset += limit

    async def call(
            self, method: str, params: Any = None, id=False, headers: dict = None,
            uri: str = None, raise_exception=True,
            unpack_response=True) -> Union[jsonrpc.RPCResponse, jsonrpc.RPCError, dict]:
        """Call a single remote RPC method.

        :param method: RPC method name
        :param params: any RPC parameters
        :param headers: headers, see `kaiju.rpc.spec` for a list of available
            headers
        :param id: optional request id
        :param raise_exception: if True, then it will rise an exception if
            any errors have been returned
        :param unpack_response: if True, then in case of valid response
            not RPCResponse objects but actual result data will be returned

        :raises RPCException: if `raise_exception` was set to True and errors
        have been returned
        """

        if uri is None:
            uri = self._uri

        request = jsonrpc.RPCRequest(id, method, params)
        headers = await self._set_headers(headers)
        response = await self._transport.rpc_request(uri=uri, request=request, headers=headers)
        response = self._process_response(response, unpack_response=unpack_response)
        if isinstance(response, jsonrpc.RPCError) and raise_exception:
            self._raise_rpc_error(response, [response], [response])
        return response

    async def call_multiple(
            self, *requests: dict, headers: dict = None, uri: str = None,
            raise_exception=True, unpack_response=True):
        """Call multiple remote RPC methods in a single batch.

        :param requests: list of request objects (see `JSONRPCHTTPClient.call`
            for each request parameters)
        :param headers: headers, see `kaiju.rpc.spec` for a list of available
            headers
        :param raise_exception: if True, then it will rise an exception if
            any errors have been returned
        :param unpack_response: if True, then in case of valid response
            not RPCResponse objects but actual result data will be returned

        :raises RPCException: if `raise_exception` was set to True and errors
            have been returned
        """

        if uri is None:
            uri = self._uri

        request = [
            jsonrpc.RPCRequest(**data)
            for data in requests
        ]
        response = await self._transport.rpc_request(uri=uri, request=request, headers=headers)
        errors, responses = self._process_response_batch(response, unpack_response=unpack_response)
        if errors and raise_exception:
            self._raise_rpc_error(errors[0], errors, responses)
        return response
