import asyncio
from time import time
from uuid import uuid4

import pytest

from ..services import JSONRPCServer
from ..jsonrpc import *
from .fixtures import *


# @pytest.mark.benchmark
# def test_rpc_server_performance(performance_test, system_information, logger):
#
#     async def _test(parallel=1, requests=1000):
#
#         logger.setLevel('ERROR')
#
#         counter = 0
#
#         async def _do_call(rpc):
#             nonlocal counter
#             headers = {
#                 JSONRPCServer.APP_ID_HEADER: uuid4()
#             }
#             data = {'method': 'do.sleep', 'params': [1, 2, 3]}
#             while 1:
#                 _id = uuid4()
#                 data['id'] = _id.int
#                 headers[JSONRPCServer.CORRELATION_ID_HEADER] = _id
#                 await rpc.call(data, headers)
#                 counter += 1
#
#         def _do_sleep(*_, **__):
#             return True
#
#         async with JSONRPCServer(logger=logger) as rpc:
#             rpc.register_namespace('do', {'sleep': _do_sleep}, {})
#             tasks = [
#                 asyncio.ensure_future(_do_call(rpc))
#                 for _ in range(parallel)
#             ]
#             t0 = time()
#             while counter < requests:
#                 await asyncio.sleep(0.1)
#             t1 = time()
#             for task in tasks:
#                 task.cancel()
#             return t1 - t0, counter
#
#     requests, parallel, n = 5000, 16, 5
#     print(f'\nJSON RPC Queued Service simple benchmark (best of {n}).')
#     print(f'{parallel} connections\n')
#     print(system_information)
#
#     dt, counter, rps = performance_test(_test, runs=n, args=(parallel, requests))
#
#     print(f'{dt}')
#     print(f'{counter} requests')
#     print(f'{rps} req/sec')


@pytest.mark.unit
async def test_rpc_server_methods(rpc_interface, rpc_compatible_service, logger):
    logger.info('Testing service context initialization.')

    async with rpc_interface as rpc:

        service = rpc_compatible_service(logger=logger)
        rpc.register_service(service.service_name, service)

        app_id = uuid4()
        correlation_id = uuid4()

        headers = {
            JSONRPCServer.APP_ID_HEADER: app_id,
            JSONRPCServer.CORRELATION_ID_HEADER: correlation_id
        }

        logger.info('Testing basic requests.')

        headers[JSONRPCServer.DEADLINE_HEADER] = int(time() + 1)
        data = {'id': uuid4().int, 'method': 'm.echo', 'params': None}
        _headers, response = await rpc.call(data, headers)
        assert _headers[JSONRPCServer.APP_ID_HEADER] == str(app_id)
        assert _headers[JSONRPCServer.CORRELATION_ID_HEADER] == str(correlation_id)
        assert response.result == ((), {})

        data = {'id': uuid4().int, 'method': 'm.echo', 'params': {'value': 42}}
        headers[JSONRPCServer.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert response.result[1]['value'] == 42

        data = {'id': uuid4().int, 'method': 'm.aecho', 'params': {'a': 1, 'b': 2, 'c': 3}}
        headers[JSONRPCServer.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert response.result[1] == {'a': 1, 'b': 2, 'c': 3}

        data = {'id': uuid4().int, 'method': 'm.echo', 'params': {'x': True}}
        headers[JSONRPCServer.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert response.result == ((), {'x': True})

        logger.info('Testing data validation.')

        data = {'id': uuid4().int, 'method': 'm.validated', 'params': {'a': 11, 'b': 2}}
        _headers, response = await rpc.call(data, headers)
        assert response.result == 22

        data = {'id': uuid4().int, 'method': 'm.validated', 'params': {'a': 11, 'b': 's'}}
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response, InvalidParams)

        # testing custom validator
        data = {'id': uuid4().int, 'method': 'm.validated_2', 'params': {'a': 11, 'b': 's'}}
        _headers, response = await rpc.call(data, headers)
        logger.debug(response)
        assert isinstance(response, InvalidParams)

        data = {'id': uuid4().int, 'method': 'm.validated', 'params': {'a': 11}}
        _headers, response = await rpc.call(data, headers)
        logger.debug(response)
        assert isinstance(response, InvalidParams)

        logger.info('Testing multi requests.')

        headers = {
            JSONRPCServer.APP_ID_HEADER: app_id,
            JSONRPCServer.CORRELATION_ID_HEADER: correlation_id
        }

        data = [
            {'id': uuid4().int, 'method': 'm.echo', 'params': {'x': True}},
            {'id': uuid4().int, 'method': 'm.echo', 'params': {'a': 1, 'b': 2}},
            {'id': uuid4().int, 'method': 'm.aecho', 'params': {'a': 1, 'b': 2}}
        ]
        headers[JSONRPCServer.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert _headers[JSONRPCServer.APP_ID_HEADER] == str(app_id)
        assert _headers[JSONRPCServer.CORRELATION_ID_HEADER] == str(correlation_id)
        assert [r.result for r in response] == [
            ((), {'x': True}),
            ((), {'a': 1, 'b': 2}),
            ((), {'a': 1, 'b': 2})
        ]

        logger.info('Testing request error handling.')

        headers = {
            JSONRPCServer.APP_ID_HEADER: app_id,
            JSONRPCServer.CORRELATION_ID_HEADER: correlation_id
        }

        data = {'id': uuid4().int, 'method': 'm.unknown', 'params': {'x': True}}
        headers[JSONRPCServer.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert _headers[JSONRPCServer.APP_ID_HEADER] == str(app_id)
        assert _headers[JSONRPCServer.CORRELATION_ID_HEADER] == str(correlation_id)
        assert isinstance(response, MethodNotFound)

        data = {'id': uuid4().int}
        headers[JSONRPCServer.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response, InvalidRequest)

        data = {'id': uuid4().int, 'method': 'm.unknown', 'params': {'value': True}, 'shit': 1}
        headers[JSONRPCServer.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response, InvalidRequest)

        data = {'id': uuid4().int, 'method': 'm.sum', 'params': {'x': 1, 'z': '2'}}
        headers[JSONRPCServer.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response, InvalidParams)

        data = {'id': uuid4().int, 'method': 'm.fail', 'params': None}
        headers[JSONRPCServer.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response, InternalError)

        logger.info('Testing timeouts.')

        data = {'id': uuid4().int, 'method': 'm.long_echo', 'params': None}
        headers[JSONRPCServer.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response, RequestTimeout)

        data = {'id': uuid4().int, 'method': 'm.long_echo', 'params': None}
        headers[JSONRPCServer.DEADLINE_HEADER] = 'sht'
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response, InvalidRequest)

        data = {'id': uuid4().int, 'method': 'm.long_echo', 'params': None}
        headers[JSONRPCServer.REQUEST_TIMEOUT_HEADER] = 4
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response, RPCResponse)

        logger.info('Testing notifications.')

        data = {'id': None, 'method': 'm.echo', 'params': None}
        headers[JSONRPCServer.DEADLINE_HEADER] = int(time() + 1)
        t = time()
        _headers, response = await rpc.call(data, headers)
        t = time() - t
        assert response is None
        assert t < 1

        logger.info('Testing for parallel task execution')

        tasks = []

        for _ in range(4):
            data = {'id': uuid4().int, 'method': 'm.standard_echo', 'params': None}
            headers[JSONRPCServer.DEADLINE_HEADER] = int(time() + 1)
            headers[JSONRPCServer.CORRELATION_ID_HEADER] = correlation_id
            tasks.append(rpc.call(data, headers))

        t = time()
        await asyncio.gather(*tasks)
        t = time() - t
        assert t <= 1

        logger.info('Testing separate bulk request error handling.')

        data = [
            {'id': None, 'method': 'm.echo', 'params': None},
            {'id': None, 'method': 'm.long_echo', 'params': None},
            {'id': uuid4().int, 'method': 'm.fail', 'params': None},
            {'id': uuid4().int, 'method': 'm.sum', 'params': {'x': 1, 'y': 2}}
        ]
        headers[JSONRPCServer.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response[0], RequestTimeout)
        assert isinstance(response[1], RPCError)
        assert response[2].result == 1 + 2

        logger.info('Testing invalid headers.')

        headers[JSONRPCServer.DEADLINE_HEADER] = 'str'
        data = {'id': uuid4().int, 'method': 'm.echo', 'params': None},
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response, InvalidRequest)

        headers[JSONRPCServer.REQUEST_TIMEOUT_HEADER] = 'str'
        data = {'id': uuid4().int, 'method': 'm.echo', 'params': None},
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response, InvalidRequest)

        headers[JSONRPCServer.APP_ID_HEADER] = 'not_uuid'
        data = {'id': uuid4().int, 'method': 'm.echo', 'params': None},
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response, InvalidRequest)

        headers[JSONRPCServer.CORRELATION_ID_HEADER] = 'not_uuid'
        data = {'id': uuid4().int, 'method': 'm.echo', 'params': None},
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response, InvalidRequest)

        logger.info('All tests finished.')
