"""
Tools for python logging.

Usage
-----

You can create a loggable class and use its personal logger (configurable).

.. code-block:: python

    from kaiju_tools.logging import Loggable

    class MyClass(Loggable):

        def __init__(self, a, b, logger=None):
            Loggable.__init__(self, logger)

    obj = MyClass(1, 2)
    obj.logger.info('Wow! Im loggin')  # will use 'MyClass' logger


To customize logging name you must rewrite `_get_logger_name` method. By
default it sets the logger name to the name of a class. You can customize it
for instance-wise behaviour for example.

.. code-block:: python

    from kaiju_tools.logging import Loggable

    class MyClass(Loggable):

        def __init__(self, *args, logger=None, **kws):
            Loggable.__init__(self, logger)

        def _get_logger_name(self):
            return f'{super().get_logger_name()}.{id(self)}'


You may pass another logger instance on init, so the class's logger will
automatically become a child of this logger (useful when your class is a part
of another class / app / test suite). By default all loggers are inherited
directly from the root logger.

.. code-block:: python

    import logging
    from kaiju_tools.logging import Loggable

    class MyClass(Loggable):
        pass

    app_logger = logging.getLogger("app")
    obj = MyClass(logger=app_logger)
    obj.logger.info('Cool!')  # will use 'app.MyClass' logger by default


Classes
-------

"""

import abc
import logging
import logging.config

__all__ = ('Loggable', 'get_logger_settings')


class Loggable(abc.ABC):
    """Allows a class to have its own logger.

    :param logger: parent logger or None for root as parent
    """

    def __init__(self, logger: logging.Logger = None):
        self._logger = self._get_logger(logger)

    @property
    def logger(self) -> logging.Logger:
        """Instance logger. By default logger name equals to the class's name."""

        return self._logger

    def _get_logger(self, logger):
        if logger:
            logger = logger.getChild(self._get_logger_name())
        else:
            logger = logging.getLogger(self._get_logger_name())
        return logger

    def _get_logger_name(self):
        return self.__class__.__name__


def get_logger_settings(level: str):
    """Default set of usefull logger settings."""

    logger = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            "default": {
                "format": '%(asctime)s %(name)s %(levelname)-8s %(message)s',
                "datefmt": '%Y-%m-%d %H:%M:%S',
            },
        },
        "handlers": {
            "console": {
                "class": 'logging.StreamHandler',
                "formatter": 'default',
                "level": level
            },
        },
        'loggers': {
            'app': {
                'handlers': ['console'],
                'level': level,
                'propagate': True
            }
        }
    }
    return logger


def init_logger(settings) -> logging.Logger:
    logging.config.dictConfig(settings)
    logger = logging.getLogger('app')
    return logger
