import os

from ..configurator import Configurator


def test_settings(logger):

    test_config = {
        'version': '1.0',
        'requirements': [
            'toml'
        ],
        'settings': {
            'main': {
                'name': '[APP_NAME]',
                'loglevel': 'DEBUG'
            },
            'app': {
                'debug': 1
            },
            'run': {
                'host': 'localhost',
                'port': 8080
            },
            'services': [
                {
                    'cls': 'JSONRPCServer',
                    'info': 'some server',
                    'settings': {}
                }
            ]
        }
    }

    custom_env = {
        'APP_NAME': 'custom_app'
    }

    configurator = Configurator(env=custom_env, logger=logger)
    config = configurator.from_dict(test_config)

    logger.debug(config)

    assert config.settings.main.name == 'custom_app'
    assert config.settings.app.debug is True
