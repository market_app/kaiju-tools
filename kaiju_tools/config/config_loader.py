"""
Config loader class is intended to be used before everything to provide an initial
config for the app and app runners.

Currently *JSON* and *YAML* formats are supported for config files.
"""

import os
import pathlib
import logging
import uuid
from argparse import ArgumentParser

from ..serialization import load
from ..services import service_class_registry
from ..logging import Loggable
from .configurator import Configurator, Config

__all__ = ('ConfigLoader', 'get_cli_parser')


def get_cli_parser() -> ArgumentParser:
    """
    Argument parser with some predefined useful parameters.
    """

    _parser = ArgumentParser(
        prog='aiohttp web application',
        description='web application run settings'
    )
    _parser.add_argument(
        '--host', dest='host', default=None,
        help='web app host (default - from settings)'
    )
    _parser.add_argument(
        '--port', dest='port', type=int, default=None,
        help='web app port (default - from settings)'
    )
    _parser.add_argument(
        '--path', dest='path', default=None, metavar='FILENAME',
        help='web app socket path (default - from settings)'
    )
    _parser.add_argument(
        '--id', dest='id', default=None,
        help='web app unique id (default - from settings)'
    )
    _parser.add_argument(
        '--debug', dest='debug', action='store_true', default=False,
        help='run in debug mode (default - from settings)'
    )
    _parser.add_argument(
        '--cfg', dest='cfg', default=None, metavar='FILENAME',
        help='web app config (default - local, fallback to base)'
    )
    _parser.add_argument(
        '--log', dest='loglevel', default=None,
        choices=tuple(logging._nameToLevel.keys()),
        help='log level'
    )
    _parser.add_argument(
        '--env-file', dest='env_file', default=None, metavar='FILENAME',
        help='custom env file'
    )
    _parser.add_argument(
        '--base-env-file', dest='base_env_file', default=None, metavar='FILENAME',
        help='custom base env file'
    )
    _parser.add_argument(
        '--env', dest='env', default=None, action='append', metavar='KEY=VALUE',
        help='overrides env variable (may be used multiple times)'
    )
    _parser.add_argument(
        '--no-os-env', dest='no_os_env', action='store_true', default=False,
        help='do not use OS environment variables'
    )
    _parser.add_argument(
        '--no-base-cfg', dest='no_base_cfg', action='store_true', default=False,
        help='do not use base config file'
    )
    _parser.add_argument(
        '--no-base-env', dest='no_base_env', action='store_true', default=False,
        help='do not use base env file'
    )
    _parser.add_argument(
        '--install-deps', dest='install_deps', action='store_true', default=False,
        help='skip installation / update of dependencies listed in config on app start'
    )
    _parser.add_argument(
        'cmd', metavar='COMMAND', default=None, nargs='?',
        help='optional management command to execute'
    )
    return _parser


class ConfigLoader(Loggable):
    """
    Config loader class. It is intended to be used before the app start.

    :param base_config_path: used as a base template
    :param default_config_path: by default this file will be used for config
    :param base_env_path: base env file
    :param default_env_path: default environment file path
    :param logger: arbitrary logger
    """

    DEFAULT_CONFIG_PATH = './config.yml'
    DEFAULT_ENV_PATH = './env.json'
    SERVICE_SUBMODULE_NAME = 'services'

    def __init__(
            self,
            base_config_path: str = DEFAULT_CONFIG_PATH,
            default_config_path: str = DEFAULT_CONFIG_PATH,
            base_env_path: str = DEFAULT_ENV_PATH,
            default_env_path: str = DEFAULT_ENV_PATH,
            logger=None):

        super().__init__(logger=logger)
        self._base_env_path = base_env_path
        self._default_env_path = default_env_path
        self._default_config_path = default_config_path
        self._base_config_path = base_config_path

    def configure(self) -> (str, Config):
        """
        Full initial app configuration. Processes CLI and OS environment arguments
        and returns a run command name and a filled config object.

        This command should ideally be used before all the imports.
        """

        args = self._parse_cli_args()
        env = self._init_env(args)
        configurator = Configurator(env=env, logger=self.logger)
        config_path = args.get('cfg')
        if not config_path:
            config_path = self._default_config_path
        self.logger.debug('Loading config from "%s".', config_path)
        if args.get('no_base_cfg'):
            base_cfg = None
        else:
            base_cfg = self._base_config_path
        config = configurator.from_file(config_path, base_cfg)
        self._set_args_from_cli(args, config)
        command = args.get('cmd')
        self._install_requirements(config, args)
        return command, config

    def _init_env(self, args):
        env_path = args.get('env_file')

        def _load_env_file(filename) -> dict:
            if not filename:
                return {}
            filename = pathlib.Path(filename)
            if not filename.exists() or filename.is_dir():
                self.logger.warning(
                    'Env path doesn\'t exist. No env will be loaded.'
                    ' "%s" - not found!', env_path)
                return {}
            with open(str(filename)) as f:
                env = load(f)
            return env

        if args.get('no_base_env'):
            env = {}
        else:
            base_env_path = args.get('base_env_file')
            if not base_env_path:
                base_env_path = self._base_env_path
            if base_env_path:
                env = _load_env_file(self._base_env_path)
            else:
                env = {}

        if not env_path:
            env_path = self._default_env_path
        if env_path and env_path != self._base_env_path:
            env.update(_load_env_file(env_path))

        if not args.get('no_os_env'):
            env.update(dict(os.environ))

        if args.get('env'):
            env.update(args['env'])

        return env

    @staticmethod
    def _parse_cli_args() -> dict:
        parser = get_cli_parser()
        args = parser.parse_args().__dict__
        env = args.get('env')
        if env:
            env_map = {}
            for record in env:
                k, v = record.split('=')
                if v.lower() == 'true':
                    v = True
                elif v.lower() == 'false':
                    v = False
                env_map[k] = v
            args['env'] = env_map
        return args

    @staticmethod
    def _set_args_from_cli(args: dict, config: Config):

        host = args.get('host')
        if host:
            config.settings.run.host = host

        port = args.get('port')
        if port:
            config.settings.run.port = port

        path = args.get('path')
        if path:
            config.settings.run.port = path

        id = args.get('id')
        if id:
            config.settings.main.id = str(uuid.UUID(id))

        debug = args.get('debug')
        if debug:
            config.settings.app.debug = debug

        log = args.get('loglevel')
        if log:
            config.settings.main.loglevel = log

    def _install_requirements(self, config, args):
        """
        Installs additional python requirements from config if needed.
        """

        if config.requirements:

            import subprocess
            import sys

            self.logger.info('Installing additional requirements...')

            for requirement in config.requirements:
                requirement = requirement.strip()
                command_args = [sys.executable, '-m', 'pip', 'install']
                if requirement.startswith('-e'):
                    command_args.extend(['-e', requirement.replace('-e', '', 1).strip()])
                    _requirement = requirement.split('#')[-1].replace('egg=', '', 1)
                else:
                    command_args.append(requirement)
                    _requirement = requirement

                if args.get('install_deps'):
                    self.logger.debug('Installing "%s".', _requirement)
                    subprocess.check_call(command_args)

                try:
                    _module = __import__(_requirement + '.' + self.SERVICE_SUBMODULE_NAME)
                except Exception:
                    pass
                else:
                    self.logger.debug('Registering services from "%s".', _requirement)
                    service_class_registry.register_classes_from_module(_module)
