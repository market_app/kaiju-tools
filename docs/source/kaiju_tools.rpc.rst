kaiju_tools.rpc
===============

JSON RPC message classes
------------------------

.. automodule:: kaiju_tools.rpc.jsonrpc
   :members:
   :undoc-members:
   :show-inheritance:

Abstract classes
----------------

.. automodule:: kaiju_tools.rpc.abc
   :members:
   :undoc-members:
   :show-inheritance:

RPC services
------------

.. automodule:: kaiju_tools.rpc.services
   :members:
   :undoc-members:
   :show-inheritance:

RPC HTTP compatible view classes
--------------------------------

.. automodule:: kaiju_tools.rpc.views
   :members:
   :undoc-members:
   :show-inheritance:

Swagger documentation generator
-------------------------------

.. automodule:: kaiju_tools.rpc.spec
   :members:
   :undoc-members:
   :show-inheritance:
